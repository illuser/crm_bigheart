from django.contrib.postgres.fields import ArrayField
from django.db import models

class Employees(models.Model):
    """
    Справочник сотрудников фонда
    """
    AVAILIBILITY_CHOICES = [
    ('Без ограничений', 'Без ограничений'),
    ('Будни, в любое время', 'Будни, в любое время'),
    ('Будни, вечер', 'Будни, вечер'),
    ('Выходные, любое время', 'Выходные, любое время'),
    ('Выходные, несколько часов', 'Выходные, несколько часов'),
    ('Другое', 'Другое'),
    ]
    name = models.CharField('Имя', max_length=50)
    birthdate = models.CharField('День рождения', max_length=50)
    city = models.CharField('Город проживания', max_length=50)
    telephone_personal = models.CharField("Телефон личн.", max_length=50)
    email_personal = models.CharField("Email личн.", max_length=50, blank=True)
    messenger = models.CharField("Мессенджер", max_length=50, blank=True)
    telephone_work = models.CharField("Телефон рабочий", max_length=50, blank=True)
    email_work = models.CharField("Email рабочий",max_length=50, blank=True)
    workplace = models.CharField("Место работы",max_length=50)
    function = models.CharField("Должность",max_length=50) 
    position = models.CharField("Позиция в штатной структуре Фонда", max_length=50)
    responsibilities = models.TextField("Зона ответственности", blank=True)
    education = models.CharField("Образование, ВУЗ",max_length=50, blank=True)
    education_type = models.CharField("Образование, специальность",max_length=50, blank=True)
    availibility =  models.CharField("Доступность",max_length=50, choices=AVAILIBILITY_CHOICES, default='Другое')
    availibility_other = models.CharField("Доступность. Описание Другое",max_length=50, blank=True)
    languages = models.CharField("Уровень владения иностр. языками",max_length=50, blank=True)
    hobby = models.TextField("Увлечения", blank=True)
    comment = models.TextField("Комментарий", blank=True)


class Wards(models.Model):
    """
    Справочник подопечных фонда
    """
    STATUS_CHOICES = [
    ('Здоров', 'Здоров'),
    ('Диагностирована болезнь', 'Диагностирована болезнь'),
    ('На лечении', 'На лечении'),
    ('Реабилитация', 'Реабилитация'),
    ('Ремиссия', 'Ремиссия')
    ]
    name = models.CharField("Имя", max_length=50)
    birthdate = models.CharField("День рождения",max_length=50, blank=True)
    status = models.CharField("Состояние",max_length=50, choices=STATUS_CHOICES)
    diagnosis = models.CharField("Диагноз",max_length=100)
    telephone_main = models.CharField("Телефон осн.",max_length=50)
    telephone_second = models.CharField("Телефон резерв.",max_length=50, blank=True)
    messenger = models.CharField("Мессенджер",max_length=50, blank=True)
    email = models.CharField("email",max_length=50, blank=True)
    occupation = models.TextField("Профессия",blank=True)
    hobby = models.TextField("Увлечения",blank=True)
    comment = models.TextField("Комментарий",blank=True) 


class Partners(models.Model):
    """
    Справочник партнеров фонда

    """
    PARTNER_TYPES = [
    ('Юридическое лицо', 'Юридическое лицо'),
    ('Физическое лицо', 'Физическое лицо'),
    ]
    PARTNER_FIELDS = [
    ('Площадка для мероприятий', 'Площадка для мероприятий'),
    ('Кейтеринг', 'Кейтеринг'),
    ('Подарки и сувениры', 'Подарки и сувениры'),
    ('Полиграфия', 'Полиграфия'),
    ('Медицинские услуги', 'Медицинские услуги'),
    ('Юридические услуги', 'Юридические услуги'),
    ('Психологи', 'Психологи'),
    ]
    PARTNER_CATEGORIES = [
    ('ВИП', 'ВИП'),
    ('Обычный', 'Обычный')
    ]
    name = models.CharField("Наименование", max_length=100)
    partner_type = models.CharField("Тип партнера",max_length=50, choices=PARTNER_TYPES)
    registration_requisites = models.TextField("Регистрационные реквизиты")
    payment_requisites = models.TextField("Платёжные реквизиты",blank=True)
    logo = models.CharField("Лого",max_length=50,blank=True)
    contacts = ArrayField(models.TextField(), verbose_name="Контактные лица", size=10)
    telephone = models.CharField("Телефон",max_length=50)
    email = models.CharField("Email",max_length=50)
    social = models.TextField("Соцсети",blank=True)
    web = models.CharField("сайт",max_length=50)
    patner_field = models.CharField("Сфера партнёрства",max_length=100, choices=PARTNER_FIELDS)
    partner_category = models.CharField("Категория",max_length=50, choices=PARTNER_CATEGORIES)
    comment = models.TextField("Комментарий",blank=True)


class Sponsors(models.Model):
    """
    Справочник спонсоров фонда
    """
    SPONSOR_TYPES = [
    ('Юридическое лицо', 'Юридическое лицо'),
    ('Физическое лицо', 'Физическое лицо'),
    ]
    SPONSOR_CATEGORIES = [
    ('ВИП', 'ВИП'),
    ('Обычный', 'Обычный')
    ]
    name = models.CharField(verbose_name="Наименование", max_length=100)
    sponsor_type = models.CharField(verbose_name="Тип спонсора",max_length=50, choices=SPONSOR_TYPES)
    registration_requisites = models.TextField(verbose_name="Регистрационные реквизиты")
    payment_requisites = models.TextField(verbose_name="Платёжные реквизиты",blank=True)
    logo = models.CharField(verbose_name="Логотип",max_length=50,blank=True)
    contacts = ArrayField(models.TextField(), verbose_name="Контактные лица", size=10)
    telephone = models.CharField(verbose_name="Телефон",max_length=50)
    email = models.CharField(verbose_name="Email",max_length=50)
    social = models.TextField(verbose_name="Соцсети",blank=True)
    web = models.CharField(verbose_name="сайт",max_length=50)
    sponsor_category = models.CharField(verbose_name="Категория",max_length=50, choices=SPONSOR_CATEGORIES)
    comment = models.TextField(verbose_name="Комментарий",blank=True)


class Benefactors(models.Model):
    """
    Справочник благотварителей фонда
    """
    BENEFACTORS_CATEGORIES = [
    ('ВИП', 'ВИП'),
    ('Обычный', 'Обычный')
    ]
    name = models.CharField("Имя", max_length=50)
    birthdate = models.CharField("День рождения", max_length=50, blank=True)
    telephone = models.CharField("Телефон",max_length=50, blank=True)
    messenger = models.CharField("Мессенджер", max_length=50, blank=True)
    email = models.CharField("Email", max_length=50)
    social = models.TextField("Соцсети",blank=True)
    web = models.CharField("сайт", max_length=50, blank=True)
    category = models.CharField("Категория", max_length=50, choices=BENEFACTORS_CATEGORIES)
    comment = models.TextField("Комментарий", blank=True)


class Activities(models.Model):
    """
    Справочник мероприятий фонда
    """
    ACTIVITY_TYPES = [
    ('Подопечные', 'Подопечные'),
    ('Бизнес', 'Бизнес')
    ]
    name = models.CharField("Наименование", max_length=50)
    activity_type = models.CharField("Тип мероприятия",max_length=50, choices=ACTIVITY_TYPES)
    date = models.DateField("Дата мероприятия")
    description = models.TextField("Описание мероприятия")
    headliner = models.CharField("Хедлайнер",max_length=50)
    place = models.CharField("Площадка", max_length=50)
    partners = models.ForeignKey(Partners, on_delete=models.CASCADE, verbose_name="Партнёры")
    sponsors = models.ForeignKey(Sponsors, on_delete=models.CASCADE, blank=True, verbose_name="Спонсоры")
    plan_num_participants = models.IntegerField("Кол-во участников план")
    fact_num_participants = models.IntegerField("Кол-во участников факт")
    rating = models.IntegerField("Оценка")

