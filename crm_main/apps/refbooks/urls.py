from django.contrib import admin
from django.urls import path, include

from .views import *

app_name = 'refbooks'

urlpatterns = [    
    path('employees', EmployeesView.as_view(template_name='refbooks.html'), name='employees_table'),
    path('employee_create', EmployeesCreateView.as_view(), name='employee_create'),
    path('employee_update/<int:pk>', EmployeesUpdateView.as_view(), name='employee_update'),
    path('employee_delete/<int:pk>', EmployeesDeleteView.as_view(), name='employee_delete'),
    
    path('wards', WardsView.as_view(template_name='refbooks.html'), name='wards_table'),
    path('wards_create', WardsCreateView.as_view(), name='wards_create'),
    path('wards_update/<int:pk>', WardsUpdateView.as_view(), name='wards_update'),
    path('wards_delete/<int:pk>', WardsDeleteView.as_view(), name='wards_delete'),
    
    path('partners', PartnersView.as_view(template_name='refbooks.html'), name='partners_table'),
    path('partners_create', PartnersCreateView.as_view(), name='partners_create'),
    path('partners_update/<int:pk>', PartnersUpdateView.as_view(), name='partners_update'),
    path('partners_delete/<int:pk>', PartnersDeleteView.as_view(), name='partners_delete'),
    
    path('sponsors', SponsorsView.as_view(template_name='refbooks.html'), name='sponsors_table'),
    path('sponsors_create', SponsorsCreateView.as_view(), name='sponsors_create'),
    path('sponsors_update/<int:pk>', SponsorsUpdateView.as_view(), name='sponsors_update'),
    path('sponsors_delete/<int:pk>', SponsorsDeleteView.as_view(), name='sponsors_delete'),
    
    path('benefactors', BenefactorsView.as_view(template_name='refbooks.html'), name='benefactors_table'),
    path('benefactors_create', BenefactorsCreateView.as_view(), name='benefactors_create'),
    path('benefactors_update/<int:pk>', BenefactorsUpdateView.as_view(), name='benefactors_update'),
    path('benefactors_delete/<int:pk>', BenefactorsDeleteView.as_view(), name='benefactors_delete'),
    
    path('activities', ActivitiesView.as_view(template_name='refbooks.html'), name='activities_table'),
    path('activities_create', ActivitiesCreateView.as_view(), name='activities_create'),
    path('activities_update/<int:pk>', ActivitiesUpdateView.as_view(), name='activities_update'),
    path('activities_delete/<int:pk>', ActivitiesDeleteView.as_view(), name='activities_delete')
]
