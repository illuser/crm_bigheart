from django.shortcuts import render
from django_filters.views import FilterView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.urls import reverse
from django_tables2.views import SingleTableView

from .models import *
from .tables import *


# ----------EMPLOYEES--------------------------------------
class EmployeesBaseView:
    def get_success_url(self):
        return reverse('refbooks:employees_table')
    
class EmployeesView(SingleTableView):
    model = Employees
    table_class = EmployeesTable
    template_name = "refbooks.html"
    #filterset_class = EmployeesFilter
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'employee_create'
        context['update_name'] = 'employee_update'
        context['refbook_name'] = 'Сотрудники/волонтеры фонда'
        return context

class EmployeesCreateView(EmployeesBaseView, CreateView):
    model = Employees
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'create_record.html'

class EmployeesUpdateView(EmployeesBaseView, UpdateView):
    model = Employees
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'update_record.html'

class EmployeesDeleteView(EmployeesBaseView, DeleteView):
    model = Employees
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'delete_record.html'
# ---------------------------------------------------------

# ----------WARDS------------------------------------------
class WardsBaseView:
    def get_success_url(self):
        return reverse('refbooks:wards_table')
    
class WardsView(SingleTableView):
    model = Wards
    table_class = WardsTable
    template_name = "refbooks.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'wards_create'
        context['update_name'] = 'wards_update'
        context['refbook_name'] = 'Подопечные'
        return context

class WardsCreateView(WardsBaseView, CreateView):
    model = Wards
    fields = ([field.name for field in model._meta.fields])
    template_name = 'create_record.html'

class WardsUpdateView(WardsBaseView, UpdateView):
    model = Wards
    fields = ([field.name for field in model._meta.fields])
    template_name = 'update_record.html'

class WardsDeleteView(WardsBaseView, DeleteView):
    model = Wards
    fields = ([field.name for field in model._meta.fields])
    template_name = 'delete_record.html'
# ---------------------------------------------------------

# ----------PARTNERS---------------------------------------
class PartnersBaseView:
    def get_success_url(self):
        return reverse('refbooks:partners_table')
    
class PartnersView(SingleTableView):
    model = Partners
    table_class = PartnersTable
    template_name = "refbooks.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'partners_create'
        context['update_name'] = 'partners_update'
        context['refbook_name'] = 'Партнеры'
        return context

class PartnersCreateView(PartnersBaseView, CreateView):
    model = Partners
    fields = ([field.name for field in model._meta.fields])
    template_name = 'create_record.html'

class PartnersUpdateView(PartnersBaseView, UpdateView):
    model = Partners
    fields = ([field.name for field in model._meta.fields])
    template_name = 'update_record.html'

class PartnersDeleteView(PartnersBaseView, DeleteView):
    model = Partners
    template_name = 'delete_record.html'
# ---------------------------------------------------------

# ----------SPONSORS---------------------------------------
class SponsorsBaseView:
    def get_success_url(self):
        return reverse('refbooks:sponsors_table')
    
class SponsorsView(SingleTableView):
    model = Sponsors
    table_class = SponsorsTable
    template_name = "refbooks.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'sponsors_create'
        context['update_name'] = 'sponsors_update'
        context['refbook_name'] = 'Спонсоры'
        return context

class SponsorsCreateView(SponsorsBaseView, CreateView):
    model = Sponsors
    fields = ([field.name for field in model._meta.fields])
    template_name = 'create_record.html'

class SponsorsUpdateView(SponsorsBaseView, UpdateView):
    model = Sponsors
    fields = ([field.name for field in model._meta.fields])
    template_name = 'update_record.html'

class SponsorsDeleteView(SponsorsBaseView, DeleteView):
    model = Sponsors
    template_name = 'delete_record.html'
# ---------------------------------------------------------

# ----------BENEFACTORS------------------------------------
class BenefactorsBaseView:
    def get_success_url(self):
        return reverse('refbooks:benefactors_table')
    
class BenefactorsView(SingleTableView):
    model = Benefactors
    table_class = BenefactorsTable
    template_name = "refbooks.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'benefactors_create'
        context['update_name'] = 'benefactors_update'
        context['refbook_name'] = 'Благотворители'
        return context

class BenefactorsCreateView(BenefactorsBaseView, CreateView):
    model = Benefactors
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'create_record.html'

class BenefactorsUpdateView(BenefactorsBaseView, UpdateView):
    model = Benefactors
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'update_record.html'

class BenefactorsDeleteView(BenefactorsBaseView, DeleteView):
    model = Benefactors
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'delete_record.html'
# ---------------------------------------------------------

# ----------ACTIVITIES-------------------------------------
class ActivitiesBaseView:
    def get_success_url(self):
        return reverse('refbooks:activities_table')
    
class ActivitiesView(SingleTableView):
    model = Activities
    table_class = ActivitiesTable
    template_name = "refbooks.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_name'] = 'activities_create'
        context['update_name'] = 'activities_update'
        context['refbook_name'] = 'Мероприятия'
        return context

class ActivitiesCreateView(ActivitiesBaseView, CreateView):
    model = Activities
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'create_record.html'

class ActivitiesUpdateView(ActivitiesBaseView, UpdateView):
    model = Activities
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'update_record.html'

class ActivitiesDeleteView(ActivitiesBaseView, DeleteView):
    model = Activities
    fields = ([field.name for field in model._meta.get_fields()])
    template_name = 'delete_record.html'
# ---------------------------------------------------------
    

    
    
