from django_tables2 import Table, LinkColumn
from django_tables2.utils import A
import django_filters

from .models import *


class EmployeesTable(Table):
    edit = LinkColumn("refbooks:employee_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:employee_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Employees
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class EmployeesFilter(django_filters.FilterSet):
    # name = django_filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = Employees
        fields = ['availibility', 'name']


class WardsTable(Table):
    edit = LinkColumn("refbooks:wards_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:wards_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Wards
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.fields])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class PartnersTable(Table):
    edit = LinkColumn("refbooks:partners_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:partners_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Partners
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.fields])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class SponsorsTable(Table):
    edit = LinkColumn("refbooks:sponsors_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:sponsors_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Sponsors
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.fields])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class BenefactorsTable(Table):
    edit = LinkColumn("refbooks:benefactors_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:benefactors_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Benefactors
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'
    

class ActivitiesTable(Table):
    edit = LinkColumn("refbooks:activities_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("refbooks:activities_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Activities
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'