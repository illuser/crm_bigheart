from django.apps import AppConfig


class RefbooksConfig(AppConfig):
    name = 'refbooks'
