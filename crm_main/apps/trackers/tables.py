from django_tables2 import Table, LinkColumn
from django_tables2.utils import A
import django_filters

from .models import *


class EmployeesTable(Table):
    edit = LinkColumn("trackers:employee_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("trackers:employee_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Employees
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class WardsTable(Table):
    edit = LinkColumn("trackers:wards_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("trackers:wards_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Wards
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class PartnersTable(Table):
    edit = LinkColumn("trackers:partners_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("trackers:partners_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Partners
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'


class BenefactorsTable(Table):
    edit = LinkColumn("trackers:benefactors_update", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Изменить")
    delete = LinkColumn("trackers:benefactors_delete", args=[A('pk')], orderable=False, empty_values=(), verbose_name="Удалить")
    class Meta:
        model = Benefactors
        template_name = "django_tables2/bootstrap.html"
        fields = ([field.name for field in model._meta.get_fields()])

    def render_edit(self):
        return 'Изменить'
    
    def render_delete(self):
        return 'Удалить'