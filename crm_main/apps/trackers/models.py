from django.contrib.postgres.fields import ArrayField
from django.db import models

import apps.refbooks.models as ref_models

class Wards(models.Model):
    """
    Запрос от подопечного
    """
    CHOICES = [
    ('Новый', 'Новый'),
    ('К исполнению', 'К исполнению'),
    ('В работе', 'В работе'),
    ('Выполнен', 'Выполнен'),
    ('Закрыт', 'Закрыт'),
    ('Отказан', 'Отказан'),
    ]
    request_id = models.IntegerField('Номер запроса', max_length=50)
    registration_date = models.DateField('Дата регистрации запроса')
    registration_time = models.TimeField('Время регистрации')
    сontact_name = models.CharField("Контактное имя", max_length=50, blank=True)
    ward_id = models.ForeignKey(ref_models.Wards, on_delete=models.CASCADE, blank=True, verbose_name="Идентификатор подопечного")
    telephone = models.CharField("Телефон",max_length=50)
    email = models.CharField("Email",max_length=50,blank=True)
    request_body = models.CharField("Тело запроса",max_length=50)
    info = models.CharField("Откуда узнали о Фонде",max_length=50,blank=True)
    status = models.CharField("Статус", max_length=50, choices=CHOICES)
    status_changed_time = models.TimeField("Дата/время перевода запроса на текущий статус")
    responsible_person = models.IntegerField("Ответственный за запрос", max_length=50, blank=True)
    executor = models.IntegerField("Исполнитель запроса", max_length=50, blank=True)
    result = models.TextField("Результат", blank=True)

class Employees(models.Model):
    """
    Запрос от волонтёра
    """
    CHOICES = [
    ('Новый', 'Новый'),
    ('К исполнению', 'К исполнению'),
    ('В работе', 'В работе'),
    ('Выполнен', 'Выполнен'),
    ('Закрыт', 'Закрыт'),
    ]
    request_id = models.IntegerField('Номер запроса', max_length=50)
    registration_date = models.DateField('Дата регистрации запроса')
    registration_time = models.TimeField('Время регистрации')
    сontact_name = models.CharField("Контактное имя", max_length=50)
    birthdate = models.CharField("День/месяц рождения", max_length=50,blank=True)
    telephone = models.CharField("Телефон",max_length=50)
    email = models.CharField("Email",max_length=50,blank=True)
    messenger = models.CharField("Мессенджер", max_length=50,blank=True)
    social = models.TextField("Соцсети",blank=True)
    web = models.CharField("сайт",max_length=50,blank=True)
    request_body = models.CharField("Тело запроса",max_length=50)
    info = models.CharField("Откуда узнали о Фонде",max_length=50,blank=True)
    status = models.CharField("Статус", max_length=50, choices=CHOICES)
    status_changed_time = models.TimeField("Дата/время перевода запроса на текущий статус")
    responsible_person = models.IntegerField("Ответственный за запрос", max_length=50, blank=True)
    executor = models.IntegerField("Исполнитель запроса", max_length=50, blank=True)
    summary = models.TextField("Отчёт", blank=True)

class Partners(models.Model):
    """
    Запрос от спонсора
    """
    CHOICES = [
    ('Новый', 'Новый'),
    ('К исполнению', 'К исполнению'),
    ('В работе', 'В работе'),
    ('Выполнен', 'Выполнен'),
    ('Закрыт', 'Закрыт'),
    ]
    request_id = models.IntegerField('Номер запроса', max_length=50)
    registration_date = models.DateField('Дата регистрации запроса')
    registration_time = models.TimeField('Время регистрации')
    сontact_name = models.CharField("Контактное имя", max_length=50)
    birthdate = models.CharField("День/месяц рождения", max_length=50,blank=True)
    telephone = models.CharField("Телефон",max_length=50)
    email = models.CharField("Email",max_length=50,blank=True)
    messenger = models.CharField("Мессенджер", max_length=50,blank=True)
    social = models.TextField("Соцсети",blank=True)
    web = models.CharField("сайт",max_length=50,blank=True)
    request_body = models.CharField("Тело запроса",max_length=50)
    info = models.CharField("Откуда узнали о Фонде",max_length=50,blank=True)
    status = models.CharField("Статус", max_length=50, choices=CHOICES)
    status_changed_time = models.TimeField("Дата/время перевода запроса на текущий статус")
    responsible_person = models.IntegerField("Ответственный за запрос", max_length=50, blank=True)
    executor = models.IntegerField("Исполнитель запроса", max_length=50, blank=True)
    summary = models.TextField("Отчёт", blank=True)

class Benefactors(models.Model):
    """
    Запрос от благотворителя
    """
    CHOICES = [
    ('Новый', 'Новый'),
    ('К исполнению', 'К исполнению'),
    ('В работе', 'В работе'),
    ('Выполнен', 'Выполнен'),
    ('Закрыт', 'Закрыт'),
    ]
    request_id = models.IntegerField('Номер запроса', max_length=50)
    registration_date = models.DateField('Дата регистрации запроса')
    registration_time = models.TimeField('Время регистрации')
    сontact_name = models.CharField("Контактное имя", max_length=50)
    birthdate = models.CharField("День/месяц рождения", max_length=50,blank=True)
    telephone = models.CharField("Телефон",max_length=50)
    email = models.CharField("Email",max_length=50,blank=True)
    messenger = models.CharField("Мессенджер", max_length=50,blank=True)
    social = models.TextField("Соцсети",blank=True)
    web = models.CharField("сайт",max_length=50,blank=True)
    request_body = models.CharField("Тело запроса",max_length=50)
    info = models.CharField("Откуда узнали о Фонде",max_length=50,blank=True)
    status = models.CharField("Статус", max_length=50, choices=CHOICES)
    status_changed_time = models.TimeField("Дата/время перевода запроса на текущий статус")
    responsible_person = models.IntegerField("Ответственный за запрос", max_length=50, blank=True)
    executor = models.IntegerField("Исполнитель запроса", max_length=50, blank=True)
    summary = models.TextField("Отчёт", blank=True)

